/**
 * Created by: Vladyslav
 * Date: 19/01/2019
 * Time: 11:44
 */
const styles = () => ({
    App: {
        position: 'absolute',
        top: '65px',
        left: 0,
        right: 0,
        bottom: '40px',
        padding: '20px',
        overflowY: 'scroll'
    }
});

export default styles;
