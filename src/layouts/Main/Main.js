/**
 * Created by: Vladyslav
 * Date: 19/01/2019
 * Time: 11:42
 */
import React, { PureComponent } from 'react';
import { withStyles } from '@material-ui/core/styles';
import styles from './MainStyles';
import { FormExpenses, TableExpenses } from '../../components';

class Main extends PureComponent {

    state = {
        items: [
            { title: 'New book about Rust', amount: 100 },
            { title: 'Snacks for a football match', amount: 20 },
            { title: 'Bus ticket', amount: 2.55 }
        ],
        rate: 4.382
    };

    constructor(props) {
        super(props);

        this.addItemHandler = this.addItemHandler.bind(this);
        this.rateHandler = this.rateHandler.bind(this);
        this.removeItemHandler = this.removeItemHandler.bind(this);
    }

    // Handle action when new item has been added in form component
    addItemHandler(item) {
        const items = Object.assign([], this.state.items);
        items.push(item);

        this.setState({ items });
    }

    // Receive index of item to delete and do deleting
    removeItemHandler(idx) {
        const items = Object.assign([], this.state.items);
        items.splice(idx, 1);

        this.setState({ items });
    }

    // Handle rate changes
    rateHandler(rate) {
        this.setState({ rate });
    }

    render() {
        const { classes } = this.props,
            { items, rate } = this.state;
        return (
            <div>
            <h2>List of expenses</h2>
                <FormExpenses rate={ rate }
                              addItem={ this.addItemHandler }
                              getRate={ this.rateHandler }/>
                <TableExpenses items={ items }
                               removeItem={ this.removeItemHandler }
                               rate={ rate }/>
            </div>
        );
    }
}

export default withStyles(styles)(Main);
