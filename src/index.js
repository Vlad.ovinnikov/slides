/**
 * Created by: Vladyslav
 * Date: 19/01/2019
 * Time: 11:33
 */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Store } from './helpers';
import App from './App';

ReactDOM.render(
    <Provider store={ Store }>
        <App/>
    </Provider>,
    document.getElementById('root')
);
