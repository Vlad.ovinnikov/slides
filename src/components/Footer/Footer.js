/**
 * Created by: Vladyslav
 * Date: 19/01/2019
 * Time: 15:42
 */
import React from 'react';
import PropTypes from 'prop-types';
import classes from './Footer.scss';
import './Footer.scss';

const Footer = props => (
    <footer className={ classes.Footer }>
        <div className={ classes.container }>
            <small>Copyright &copy; { props.title }</small>
        </div>
    </footer>
);

Footer.propTypes = {
    title: PropTypes.string.isRequired
};

export default Footer;
