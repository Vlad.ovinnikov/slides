/**
 * Created by: Jax
 * Date: 15/10/2018
 * Time: 18:26
 */
import React from 'react';
import _ from 'lodash';
import { withStyles } from '@material-ui/core/styles';
import { FormControl, InputLabel, Input, FormHelperText } from '@material-ui/core';
import styles from './InputFieldStyles';

const inputField = ({ classes, input, label, type, meta: { touched, error } }) => {
    return (
        <FormControl className={ classes.formControl }
                     fullWidth
                     error={ touched && !_.isEmpty(error) }
                     aria-describedby={ `component-${ label }-text` }>
            <InputLabel className={ classes.label }
                        htmlFor={ `component-${ label }` }>{ label }</InputLabel>
            <Input id={ `component-${ label }` }
                   style={ { marginTop: '10px' } }
                   placeholder={ label }
                   type={ type }
                   { ...input }/>
            { touched && error &&
            <FormHelperText id={ `component-${ label }-text` }>{ error }</FormHelperText> }
        </FormControl>
    );
};

export default withStyles(styles)(inputField);
