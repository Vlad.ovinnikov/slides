/**
 * Created by: Vladyslav
 * Date: 19/01/2019
 * Time: 11:44
 */
import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';

export default combineReducers({
    form: formReducer
});
