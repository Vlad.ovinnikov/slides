{
  "env": {
    "browser": true,
    "es6": true,
    "node": true
  },
  "globals": {
    "System": false,
    "opr": false,
    "safari": false,
    "document": false,
    "escape": false,
    "navigator": false,
    "unescape": false,
    "window": true,
    "describe": true,
    "before": true,
    "it": true,
    "expect": true,
    "sinon": true,
    "process": true
  },
  "parserOptions": {
    "ecmaFeatures": {
      "jsx": true
    }
  },
  "extends": [
    "eslint:recommended",
    "plugin:react/recommended"
  ],
  "parser": "babel-eslint",
  "plugins": [
    "react"
  ],
  "settings": {
    "react": {
      "createClass": "createReactClass",
      // Regex for Component Factory to use,
      // default to "createReactClass"
      "pragma": "React",
      // Pragma to use, default to "React"
      "version": "16.6",
      // React version, default to the latest React stable release
      "flowVersion": "6.3"
      // Flow version
    },
    "propWrapperFunctions": [
      "forbidExtraProps"
    ]
    // The names of any functions used to wrap the
    // propTypes object, e.g. `forbidExtraProps`.
    // If this isn't set, any propTypes wrapped in
    // a function will be skipped.
  },
  "rules": {
    "react/prop-types": 0,
    "react/jsx-curly-spacing": [
      2,
      "always"
    ],
    "react/jsx-no-target-blank": [
      1,
      {
        "enforceDynamicLinks": "never"
      }
    ],
    "react/jsx-uses-react": "error",
    "react/jsx-uses-vars": "error",
    "react/no-unescaped-entities": [
      "error",
      {
        "forbid": [
          ">",
          "}"
        ]
      }
    ],
    "react/display-name": [
      0,
      {
        "ignoreTranspilerName": true
      }
    ],
    "react/no-find-dom-node": 0,
    "block-scoped-var": 2,
    "brace-style": [
      2,
      "1tbs",
      {
        "allowSingleLine": true
      }
    ],
    "camelcase": 0,
    "comma-dangle": [
      2,
      "never"
    ],
    "computed-property-spacing": [
      2,
      "never"
    ],
    "comma-spacing": [
      2,
      {
        "before": false,
        "after": true
      }
    ],
    "comma-style": [
      2,
      "last"
    ],
    "complexity": 0,
    "consistent-return": 0,
    "consistent-this": 1,
    "curly": [
      2,
      "multi-line"
    ],
    "default-case": 2,
    "dot-location": [
      2,
      "property"
    ],
    "dot-notation": 2,
    "eol-last": 0,
    "eqeqeq": [
      2,
      "allow-null"
    ],
    "func-names": 0,
    "func-style": 0,
    "generator-star-spacing": [
      2,
      "both"
    ],
    "guard-for-in": 2,
    "handle-callback-err": [
      2,
      "^(err|error|anySpecificError)$"
    ],
    "indent": [
      0,
      4,
      {
        "SwitchCase": 1
      }
    ],
    "arrow-body-style": [
      0,
      "as-needed"
    ],
    "arrow-parens": [
      2,
      "as-needed"
    ],
    "arrow-spacing": 2,
    "key-spacing": [
      2,
      {
        "beforeColon": false,
        "afterColon": true
      }
    ],
    "keyword-spacing": [
      2,
      {
        "before": true,
        "after": true
      }
    ],
    "linebreak-style": 1,
    "max-depth": 1,
    "max-len": [
      2,
      220,
      {
        "ignoreStrings": true,
        "ignoreTemplateLiterals": true,
        "ignoreComments": true
      }
    ],
    "max-nested-callbacks": 1,
    "max-params": 0,
    "max-statements": 0,
    "new-cap": [
      2,
      {
        "newIsCap": true,
        "capIsNew": false
      }
    ],
    "newline-after-var": [
      0,
      "never"
    ],
    "new-parens": 2,
    "no-alert": 0,
    "no-array-constructor": 2,
    "no-bitwise": 0,
    "no-caller": 2,
    "no-confusing-arrow": 2,
    "no-catch-shadow": 2,
    "no-cond-assign": 2,
    "no-const-assign": 2,
    "no-console": 0,
    "no-constant-condition": 0,
    "no-continue": 0,
    "no-control-regex": 2,
    "no-debugger": 2,
    "no-delete-var": 2,
    "no-div-regex": 2,
    "no-dupe-args": 2,
    "no-dupe-keys": 2,
    "no-duplicate-case": 2,
    "no-else-return": 2,
    "no-empty": 1,
    "no-empty-character-class": 2,
    "no-eq-null": 0,
    "no-eval": 2,
    "no-ex-assign": 2,
    "no-extend-native": 2,
    "no-extra-bind": 2,
    "no-extra-boolean-cast": 2,
    "no-extra-parens": 0,
    "no-extra-semi": 2,
    "no-extra-strict": 0,
    "no-fallthrough": 2,
    "no-floating-decimal": 2,
    "no-func-assign": 2,
    "no-implied-eval": 2,
    "no-inline-comments": 0,
    "no-inner-declarations": [
      2,
      "functions"
    ],
    "no-invalid-regexp": 2,
    "no-irregular-whitespace": 2,
    "no-iterator": 2,
    "no-label-var": 2,
    "no-labels": 2,
    "no-lone-blocks": 2,
    "no-lonely-if": 2,
    "no-loop-func": 2,
    "no-mixed-requires": 1,
    "no-mixed-spaces-and-tabs": 2,
    "no-multi-spaces": 2,
    "no-multi-str": 2,
    "no-multiple-empty-lines": [
      2,
      {
        "max": 1
      }
    ],
    "no-native-reassign": 2,
    "no-negated-in-lhs": 2,
    "no-nested-ternary": 2,
    "no-new": 2,
    "no-new-func": 2,
    "no-new-object": 2,
    "no-new-require": 2,
    "no-new-wrappers": 2,
    "no-obj-calls": 2,
    "no-octal": 2,
    "no-octal-escape": 2,
    "no-path-concat": 0,
    "no-plusplus": 0,
    "no-prototype-builtins": 2,
    "template-curly-spacing": [
      2,
      "always"
    ],
    "prefer-template": 2,
    "no-process-env": 0,
    "no-process-exit": 0,
    "no-proto": 2,
    "no-redeclare": 2,
    "no-regex-spaces": 2,
    "no-reserved-keys": 0,
    "no-restricted-modules": 1,
    "no-return-assign": 2,
    "no-script-url": 2,
    "no-self-compare": 2,
    "no-sequences": 2,
    "no-shadow": 2,
    "no-shadow-restricted-names": 2,
    "no-spaced-func": 2,
    "no-sparse-arrays": 2,
    "no-sync": 1,
    "no-ternary": 0,
    "no-throw-literal": 2,
    "no-trailing-spaces": 0,
    "no-undef": 2,
    "no-undef-init": 2,
    "no-undefined": 2,
    "no-underscore-dangle": 0,
    "no-unneeded-ternary": 2,
    "no-unreachable": 2,
    "no-unused-expressions": 2,
    "no-unused-vars": [
      1,
      {
        "vars": "all",
        "let": "all",
        "const": "all",
        "args": "after-used",
        "caughtErrors": "none"
      }
    ],
    "no-use-before-define": 2,
    "no-var": 2,
    "no-void": 0,
    "no-warning-comments": 0,
    "no-with": 2,
    "one-var": 0,
    "no-useless-escape": 0,
    "prefer-arrow-callback": 2,
    "operator-assignment": [
      2,
      "always"
    ],
    "operator-linebreak": [
      2,
      "after"
    ],
    "padded-blocks": [
      0,
      {
        "blocks": "as-needed",
        "classes": "as-needed",
        "switches": "as-needed"
      }
    ],
    "quotes": [
      2,
      "single",
      "avoid-escape"
    ],
    "quote-props": [
      2,
      "as-needed"
    ],
    "radix": [
      2,
      "always"
    ],
    "semi": [
      2,
      "always"
    ],
    "semi-spacing": [
      2,
      {
        "before": false,
        "after": true
      }
    ],
    "sort-vars": [
      0,
      {
        "ignoreCase": true
      }
    ],
    "space-before-blocks": 2,
    "object-curly-spacing": [
      2,
      "always"
    ],
    "array-bracket-spacing": [
      2,
      "never"
    ],
    "space-before-function-paren": [
      1,
      {
        "anonymous": "always",
        "named": "never"
      }
    ],
    "space-in-brackets": 0,
    "space-in-parens": 2,
    "space-infix-ops": 2,
    "space-unary-ops": [
      2,
      {
        "words": true,
        "nonwords": false
      }
    ],
    "spaced-comment": 2,
    "strict": [
      2,
      "global"
    ],
    "use-isnan": 2,
    "valid-jsdoc": 0,
    "valid-typeof": 2,
    "vars-on-top": 2,
    "wrap-iife": 2,
    "wrap-regex": 1,
    "yoda": 2,
    "prefer-const": 1
  }
}
